<div align="center">
  <a href="#">
    <img src="https://res.cloudinary.com/eoi-digital/image/upload/v1629143565/defi_bridge_dao_logo_text.svg_kuhpun.svg" alt="Logo" height="160">
  </a>
  <h1>RWAT = FiatUSD</h1>
<p>Stablecoin 2.0 with 1:1 backing</p>
</div>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-defi-bridge-dao">About DeFi Bridge DAO</a></li>
    <li><a href="#high-level-overview-cefidefi-merge">High-level overview CeFi/DeFi merge</a></li>
    <li><a href="#minting-and-redeeming-process-fiatusd-rwat">Minting and Redeeming process</a></li>
    <li><a href="#testing-and-development">Testing and Development</a></li>
  </ol>
</details>

<!-- About DeFi Bridge DAO -->
## About DeFi Bridge DAO
DeFi Bridge DAO (DBD)🌉 brings Real World assets on-chain (Real World Asset Tethers, RWAT) starting with the most important and challenging of them all: FIAT. While building FluidFi and looking for ways to wrap Fiat and use it on-chain we found a way to represent Off-Chain assets on-chain. By combining the latest DeFi money Legos in pragmatic new ways we are able to do this.

DBD, in collaboration with FluidFi, will bring 4 stablecoins on-chain as our DAO's flagship product. We call it fiatUSD (Fully Insured Asset Tethered USD) and we will start with fiatUSD and later also fiatEUR, fiatCHF, and fiatGBP. We could see these assets as real-world asset tethers (RWAT) minted on blockchains so that users can leverage all the possibilities a new blockchain technology stack has to offer, yet are always redeemable 1:1 for their offchain version. We are progressively decentralizing and our goal is to separate the mint process and its fee parameters into a fully transparent DAO. We want to give our users full transparency, trust, and control - the DBD node monitors a FluidFi/DeFi Bridge DAO treasury account which holds off-chain FIAT sent in by users, and the DBD will mint a digital representation in the form of Fiat (Insured Asset Tethered) to the user’s verified blockchain address.


<!-- High-level overview CeFi/DeFi merge -->
## High-level overview CeFi/DeFi merge
<img src="https://res.cloudinary.com/eoi-digital/image/upload/v1631179480/Money_Flow_jgkizw.jpg" alt="High-level overview CeFi/DeFi merge" height="460">

Here is a more in-depth view of the process where we can see a smart contract that looks at the Fiat Treasury Account and will mint assets to the user’s whitelisted blockchain address. Redemption of assets can flow in a similar way: where users with a whitelisted blockchain address can redeem Fiat to their bank accounts, it then is up to the CeFi banking side to run all the compliance checks. For extra transparency and trust, we use oracles in our contracts to make it impossible by design to mint more than there are FIAT assets in the Treasury Account; this makes the system more secure and transparent.


<!-- Minting and Redeeming process -->
## Minting and Redeeming process FiatUSD RWAT

### High Level Overview
#### Mint
1. User moves funds from his bank account to the fiatUSD treasury bank account
2. DAO Validator node monitors the fiatUSD treasury account, sees the new inflow and updates the OffchainTreasuryOracle 
3. A corresponding amount of fiatUSD token is minted to the user's wallet

#### Redeem
1. User calls the `redeem` function in the fiatUSD smart contract
2. The contract burns the specified amount of token
3. DAO Validator node monitors the contract for the `Redeem` event and moves a corresponding amount of USD from the fiatUSD treasury bank account to the users bank account.

<img src="https://res.cloudinary.com/eoi-digital/image/upload/v1629150509/fiatUSD_DeFi_sd_kr8cve.jpg" alt="Minting and Redeeming process" height="660">

The DeFi Bridge DAO, in collaboration with FluidFi, will start by minting a stablecoin called fiatUSD (Fully Insured Asset Tethered USD) via its generalized RWAT Mint/Redeem Process.

### Detailed Mint/Redeem Description

Involved contracts: 
* `RealWorldAssetTether.sol` 
* `OffchainTreasuryOracle.sol`
* `CrossChainTotalSupplyOracle.sol` (only indirectly in the collateralization check)

`RealWorldAssetTether` is the generalized contract and an instance will be deployed as `fiatUSD`.

#### Mint
1. The DAO validator node picks up a new deposit in the treasury bank account
```javascript
{
  amount: 1000.00,  // amount of USD deposited
  address: '0x123', // user has proven ownership of this wallet
  chain: 'ETHEREUM', // the chain the user wants to receive the tokens on
  txID: '111-222-333-444-1', // CeFi transaction ID
  // solidity compatible ECDSA signature over the above 4 values signed with the CeFi partners private key
  signature: '9cb87877a736922a6c7a8f6ef5c3ec32536835e738683daa0b300fcd2a447ebf09c0f4ec144361d713ece0a81230ac3bd161420fe1c95c9bc4376a02329474751b', 
},
```

2. The DAO Validator node calls `update` on the `OffchainTreasuryOracle` contract
```solidity
function update(
    int256[] calldata _amounts, 
    address[] calldata _addresses, 
    bytes32[] calldata _cefiTxIDs,
    uint256[] calldata _chainIDs,
    bytes[] calldata _signatures
  ) external onlyRole(ORACLE_UPDATER_ROLE)
```

3. The OffchainTreasuryOracle contract checks the signature and ensures it was signed by the `offchainSigner` address
4. The OffchainTreasuryOracle contract updates the internal `balance` and calls `mint` in the associated `tokenContract` (fiatUSD)
5. The `tokenContract` checks collateralization
```solidity
require(
  (crossChainTotalSupplyOracle.getLatestAnswer() + totalPendingRedeems + _amount) <= offchainTreasuryOracle.getLatestAnswer(),
  "totalSupply cannot be greater than offchainAssetBalance"
);
```
6. The `tokenContract` mints the tokens to the user's associated wallet.


#### Redeem
1. A user calls `redeem` on the `RealWorldAssetTether` (fiatUSD) contract 
2. The contract burns the tokens 
3. The contract keeps track of the `totalPendingRedeems` for the collateralization check
4. The contract emits a `Redeem` event
5. The DAO Validator node picks up the `Redeem` event and transfers a corresponding amount of USD from the offchain treasury bank account to the users bank account.
6. The DAO Validator node calls `update` on the `OffchainTreasuryOracle` contract with the new outflow transaction
7. The `update` call is validated and calls `releasePendingRedeem` on the `tokenContract` (fiatUSD)
8. `releasePendingRedeem` reduces `totalPendingRedeems` so the `totalSupply + totalPendingRedeems` is in sync with the `OffchainTreasuryOracle.balance`

## Bridge
`fiatUSD` will be deployed on multiple EVM compatible chains (L1 or L2).
To move fiatUSD tokens from one chain to another chain the process is:
1. user calls `bridgeOut` function on the source chain
2. `bridgeOut` burns the tokens on this chain (reducing the `crossChainTotalSupply`)
3. `bridgeOut` emits a `BridgeOut` event
```solidity
emit BridgeOut(msg.sender, _to, _amount, _destChainID);
```
4. `BridgeOut` event is picked up by DAO Validator node
5. DAO Validator node waits for transaction to be finalized and `CrossChainTotalSupplyOracle` to be updated
6. DAO Validator node calls `batchBridgeIn` on the destination chain and mints the tokens on this chain to the recipient address.

<!-- Testing and Development -->
## Testing and Development

Contracts are built on top of `@openzeppelin/contracts` and `@openzeppelin/contracts-upgradeable` using hardhat and ethereum-waffle.

To test or develop checkout the repo and run
```
npm install
```


# Deployment
## CrossChainTotalSupplyOracle
```
$ npx hardhat run --network kovan scripts/deploy-crossChainOracle.js
Deploying CrossChainTotalSupplyFeed.
oracle deployed to:  0x...
```
```
await crossChainTotalSupplyOracle.grantRole(ORACLE_UPDATER_ROLE, '0x..')
```

## OffchainTreasuryOracle
```
$ npx hardhat run --network kovan scripts/deploy-offchainTreasury.js 
Deploying OffchainTreasury.
offchainTreasury deployed to:  0x...
```


## fiatUSD
```
$ export VERIFIED_OFFCHAIN_ASSET_BALANCE_ORACLE_ADDRESS=0x123
$ export CROSS_CHAIN_TOTAL_SUPPLY_ORACLE_ADDRESS=0x456
$ npx hardhat run --network arbitrumtest scripts/deploy-fiatUSD.js
Deploying fiatUSD with
VERIFIED_OFFCHAIN_ASSET_BALANCE_ORACLE_ADDRESS: 0x123
CROSS_CHAIN_TOTAL_SUPPLY_ORACLE_ADDRESS: 0x456
Proceed with these settings? (y/N): y
deploying contract...
fiatUSD deployed to:  0x...
```

## Final Configuration
Set fiatUSD contract in offchainTreasuryOracle:
```
await offchainTreasuryOracle.updateTokenContract('0x...')
```

Give DBD node address ORACLE_UPDATER_ROLE in offchainTreasuryOracle
```
await offchainTreasuryOracle.grantRole(ORACLE_UPDATER_ROLE, '0x...')
```