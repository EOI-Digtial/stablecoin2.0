const { expect } = require('chai');

const ORACLE_UPDATER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('ORACLE_UPDATER_ROLE'))

const REVERT_UNKNOWN_CHAIN = 'unknown chain'

const CHAIN_ID_ETHEREUM = 31337 // hardhat uses this
const CHAIN_ID_POLYGON = 137 // https://magic.link/docs/blockchains/polygon

describe('CrossChainTotalSupplyOracle', function() {

  let CrossChainTotalSupplyOracleFactory
  let oracle
  let owner
  let oracleUpdater
  let NONoracleUpdater
  let addrs

  before(async function() {
    CrossChainTotalSupplyOracleFactory = await ethers.getContractFactory('CrossChainTotalSupplyOracle')
  })

  beforeEach(async function() {
    [owner, oracleUpdater, NONoracleUpdater, ...addrs] = await ethers.getSigners()

    oracle = await CrossChainTotalSupplyOracleFactory.deploy()

    await oracle.grantRole(ORACLE_UPDATER_ROLE, oracleUpdater.address)
  })

  describe('Getting a value', async function() {
    const initialValue = 100
    let initalUpdatedBlockTimestamp
    beforeEach(async function() {
      await oracle.connect(oracleUpdater).setValue(initialValue, [CHAIN_ID_ETHEREUM, CHAIN_ID_POLYGON], [123, 456])
      initalUpdatedBlockTimestamp = (await ethers.provider.getBlock()).timestamp
    })
    it('does not revert', async function() {
      const getCall = oracle.getLatestAnswer()
      await expect(getCall).to.not.be.reverted
    })
    it('should return a value', async function() {
      const value = await oracle.getLatestAnswer()
      expect(value).to.equal(initialValue)
    })
    it('should return an updatedAt', async function() {
      const updatedAt = await oracle.getLatestAnswerTime()
      expect(updatedAt).to.equal(initalUpdatedBlockTimestamp)
    })
  })

  describe('Setting a new value', async function() {
    const initialValue = 100
    let initalUpdatedBlockTimestamp
    beforeEach(async function() {
      await oracle.connect(oracleUpdater).setValue(initialValue, [CHAIN_ID_ETHEREUM, CHAIN_ID_POLYGON], [123, 456])
      initalUpdatedBlockTimestamp = (await ethers.provider.getBlock()).timestamp
    })
    specify('calling setValue changes the value', async function() {
      const beforeUpdate = await oracle.getLatestAnswer()
      expect(beforeUpdate).to.equal(initialValue)

      const newValue = 123
      expect(newValue).to.not.equal(initialValue)
      
      const updateCall = oracle.connect(oracleUpdater).setValue(newValue, [CHAIN_ID_ETHEREUM, CHAIN_ID_POLYGON], [123, 456])
      await expect(updateCall).to.not.be.reverted
      
      const afterUpdate = await oracle.getLatestAnswer()
      expect(afterUpdate).to.equal(newValue)
    })
    specify('calling setValue sets the updatedAt to current block timestamp', async function() {
      const beforeUpdate = await oracle.getLatestAnswerTime()
      expect(beforeUpdate).to.equal(initalUpdatedBlockTimestamp)
      
      const newValue = 123
      const updateCall = oracle.connect(oracleUpdater).setValue(newValue, [CHAIN_ID_ETHEREUM, CHAIN_ID_POLYGON], [123, 456])
      await expect(updateCall).to.not.be.reverted
      
      const currentBlockTimestamp = (await ethers.provider.getBlock()).timestamp
      const afterUpdate = await oracle.getLatestAnswerTime()
      expect(afterUpdate).to.equal(currentBlockTimestamp)
    })
    specify('calling with invalid arguments reverts', async function() {
      const updateCall = oracle.connect(oracleUpdater).setValue(1, [CHAIN_ID_ETHEREUM], [123, 456])
      await expect(updateCall).to.be.reverted
    })
  })
  
  describe('chains and blocknumbers', async function() {
    specify('calling setValue sets the chains and corresponding blocknumbers correctly', async function() {
      const updateCall = oracle.connect(oracleUpdater).setValue(1, [CHAIN_ID_ETHEREUM, CHAIN_ID_POLYGON], [123, 456])
      await expect(updateCall).to.not.be.reverted

      const ethBlockNumber = await oracle.getSnapshotBlocknumber(CHAIN_ID_ETHEREUM)
      expect(ethBlockNumber).to.equal(123)
      const polygonBlockNumber = await oracle.getSnapshotBlocknumber(CHAIN_ID_POLYGON)
      expect(polygonBlockNumber).to.equal(456)
    })
    specify('checking blocknumber for an invalid chain reverts', async function() {
      const call = oracle.getSnapshotBlocknumber(3)
      await expect(call).to.be.revertedWith(REVERT_UNKNOWN_CHAIN)
    })
  })

})