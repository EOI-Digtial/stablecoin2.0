// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;

import "../interfaces/IOracleFeed.sol";
import "../RealWorldAssetTether.sol";

// simplyfied oracle that always returns `tokenContract.totalSupply()`
// while RWAT is only deployed on a single chain.
contract SingleChainTotalSupplyOracle is IOracleFeed {

  RealWorldAssetTether public tokenContract;
  
  constructor(address _tokenContract) {
    require(_tokenContract != address(0), "_tokenContract is 0x0");
    tokenContract = RealWorldAssetTether(_tokenContract);
  }

  // returns tokenContract.totalSupply()
  function getLatestAnswer() external view override returns (uint) {
    return tokenContract.totalSupply();
  }
  
  // is always up to date
  function getLatestAnswerTime() external view override returns (uint) {
    return block.timestamp;
  }
}