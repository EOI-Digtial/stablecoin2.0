// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "../interfaces/IOracleFeed.sol";

contract CrossChainTotalSupplyOracle is AccessControl, IOracleFeed {
  bytes32 public constant ORACLE_UPDATER_ROLE = keccak256("ORACLE_UPDATER_ROLE");

  uint256 private value;
  uint256 private updatedAt;

  // chainID => snapshotBlockNr
  mapping (uint256 => uint256) snapshotBlockNrByChainID;
  
  uint256[] chainIDs;
  uint256[] snapshotBlockNrs;

  event SetValue(uint value, uint256[] chainIDs, uint256[] blockNrs);
  
  constructor() {
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
  }

  /** 
  * @dev updates the `value` and sets the `updatedAt` to the current block.number.
  * we also keep track of the blocknumbers for all involved chains this new value is
  * valid for.
  * @param _value the latest value 
  * @param _chainIDs all chains that contributed to `_value`
  * @param _snapshotBlockNrs highest blocknumber of the `_chains` that `value` is valid for
  */
  function setValue(uint256 _value, uint256[] calldata _chainIDs, uint256[] calldata _snapshotBlockNrs) external onlyRole(ORACLE_UPDATER_ROLE) {
    require(
      _chainIDs.length == _snapshotBlockNrs.length,
      "invalid arguments: same number of _chainIDs and _blockNrs must be specified"
    );
    
    value = _value;
    updatedAt = block.timestamp;
    
    chainIDs = _chainIDs;
    snapshotBlockNrs = _snapshotBlockNrs;
    
    emit SetValue(_value, _chainIDs, _snapshotBlockNrs);
  }

  /**
  * @dev returns the blocknumber of `_chainID` that `value` is valid for
  */
  function getSnapshotBlocknumber(uint _chainID) public view returns (uint) {    
    for (uint i = 0; i < chainIDs.length; i += 1) {
      if (chainIDs[i] == _chainID) return snapshotBlockNrs[i];
    }
    
    revert("unknown chain");
  }
  
  function getLatestAnswer() external view override returns (uint) {
    return value;
  }
  
  function getLatestAnswerTime() external view override returns (uint) {
    return updatedAt;
  }
}