const { ethers } = require('hardhat')

async function main() {
  const OracleFactory = await ethers.getContractFactory('SingleChainTotalSupplyOracle')
  console.log('Deploying SingleChainTotalSupplyOracle.')

  console.log('account:', (await ethers.getSigner()).address)

  let tokenContract = process.env.RWAT_TOKEN_CONTRACT_ADDRESS
  if(!tokenContract) {
    console.log('RWAT_TOKEN_CONTRACT_ADDRESS not set')
    return
  }
  
  const oracle = await OracleFactory.deploy(tokenContract)
  
  await oracle.deployed();
  console.log(`oracle deployed to: `, oracle.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });