const { ethers, upgrades } = require('hardhat');

async function main() {
  const OracleFactory = await ethers.getContractFactory('DaoOracleFeed');
  console.log('Deploying upgradable DaoOracleFeed.');
  
  const oracle = await upgrades.deployProxy(
    OracleFactory, 
    [],
    { initializer: 'initialize()' }
  );
  
  await oracle.deployed();
  console.log(`oracle deployed to: `, oracle.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });