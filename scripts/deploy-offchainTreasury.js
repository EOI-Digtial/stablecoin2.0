const { ethers } = require('hardhat');

const UPDATER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('ORACLE_UPDATER_ROLE'))
const ADMIN_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('ORACLE_ADMIN_ROLE'))

async function main() {
  const OffchainTreasury = await ethers.getContractFactory('OffchainTreasuryOracle');
  console.log('Deploying OffchainTreasury.');

  const offchainTreasury = await OffchainTreasury.deploy('');
  console.log(`offchainTreasury deployed to: `, offchainTreasury.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });