const { ethers } = require('hardhat')

async function main() {
  const OracleFactory = await ethers.getContractFactory('CrossChainTotalSupplyOracle')
  console.log('Deploying CrossChainTotalSupplyFeed.')

  console.log('account:', (await ethers.getSigner()).address)
  
  const oracle = await OracleFactory.deploy()
  
  await oracle.deployed();
  console.log(`oracle deployed to: `, oracle.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });