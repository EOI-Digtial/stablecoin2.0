const { ethers, upgrades } = require('hardhat');

const readline = require('readline');

async function main() {

  let verifiedOffchainAssetBalanceOracleAddress = process.env.VERIFIED_OFFCHAIN_ASSET_BALANCE_ORACLE_ADDRESS
  let crossChainTotalSupplyOracleAddress = process.env.CROSS_CHAIN_TOTAL_SUPPLY_ORACLE_ADDRESS

  if(!verifiedOffchainAssetBalanceOracleAddress) {
    throw('env VERIFIED_OFFCHAIN_ASSET_BALANCE_ORACLE_ADDRESS needs to be set')
  }
  if(!crossChainTotalSupplyOracleAddress) {
    throw('env CROSS_CHAIN_TOTAL_SUPPLY_ORACLE_ADDRESS needs to be set')
  }

  const FiatUSDFactory = await ethers.getContractFactory('RealWorldAssetTether');
  console.log('Deploying fiatUSD with');
  console.log(`VERIFIED_OFFCHAIN_ASSET_BALANCE_ORACLE_ADDRESS: ${verifiedOffchainAssetBalanceOracleAddress}`)
  console.log(`CROSS_CHAIN_TOTAL_SUPPLY_ORACLE_ADDRESS: ${crossChainTotalSupplyOracleAddress}`)
  
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.question('Proceed with these settings? (y/N): ', async (answer) => {
    if(answer != 'y' && answer != 'Y') {
      console.error('aborted by user')
      process.exit(1)
    }

    console.log('deploying contract...')

    const fiatUSD = await upgrades.deployProxy(
      FiatUSDFactory, 
      [
        'FluidFi FiatUSD', 
        'fiatUSD', 
        verifiedOffchainAssetBalanceOracleAddress, 
        crossChainTotalSupplyOracleAddress,
        '' // whitelistSigner
      ], 
    );
    
    await fiatUSD.deployed();
    console.log(`fiatUSD deployed to: `, fiatUSD.address);
  
    rl.close();
  });

}

try {
  main()
}
catch(error) {
  console.error(error);
  process.exit(1);
}