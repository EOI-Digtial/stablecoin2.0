require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");

require("hardhat-gas-reporter");

require('@openzeppelin/hardhat-upgrades');

require("hardhat-watcher");

require('solidity-coverage');

const { alchemyApiKey, mnemonic, etherscanApiKey } = require('./secrets.json');

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  networks: {
    kovan: {
      url: `https://eth-kovan.alchemyapi.io/v2/${alchemyApiKey}`,
      accounts: {mnemonic: mnemonic}
    },
    mumbai: {
      url: "https://rpc-mumbai.maticvigil.com",
      accounts: {mnemonic: mnemonic}
    },
    arbitrumtest: {
      url: "https://rinkeby.arbitrum.io/rpc",
      accounts: {mnemonic: mnemonic},
    },
    hardhat: {
      initialBaseFeePerGas: 0 // hack needed to make solidity-coverage work on LONDON
    }
  },
  etherscan: {
    apiKey: `${etherscanApiKey}`
  },
  solidity: {
    version: "0.8.9",
    settings: {
      outputSelection: {
        "*": {
          "*": ["storageLayout"],
        },
      },
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  watcher: {
    test: {
      tasks: [{ command: 'test', params: { testFiles: ['{path}'] } }],
      files: ['./test/**/*'],
      verbose: true
    }
  },
  gasReporter: {
    currency: 'USD',
    coinmarketcap: '',
    enabled: (process.env.REPORT_GAS) ? true : false
  }
};

